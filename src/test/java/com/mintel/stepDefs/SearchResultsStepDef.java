package com.mintel.stepDefs;

import org.junit.Assert;

import com.mintel.data.Constants.ProjectLabel;
import com.mintel.pages.SearchResultsPage;
import com.mintel.utilities.ScenarioContext;
import com.mintel.utilities.TestContext;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Steps relating to the search results feature are found here.
 */
public class SearchResultsStepDef
{
    private SearchResultsPage searchResultsPage;
    private ScenarioContext scenarioContext;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Initialise page classes into objects
     * <p>
     * @param testContext Allows usage of TestContext methods to initialise pages/driver
     */
    public SearchResultsStepDef(TestContext testContext)
    {
        searchResultsPage = testContext.getPageObjectManager().getSearchResultsPage();
        scenarioContext = testContext.getScenarioContext();
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Click a button on a piece
     * <p>
     * @param buttonName Button name to click
     * @param pieceType Type of piece to look within
     */
    @When("^I click (.*) for (.*)$")
    public void clickButtonInSearchList(String buttonName, String pieceType)
    {
        searchResultsPage.clickButtonInSearchList(buttonName, pieceType, scenarioContext);
    }

    /**
     * Click the view cart button within the message
     */
    @When("^I click View cart within the message$")
    public void clickButtonInSearchList()
    {
        searchResultsPage.clickViewCart();
    }

    /**
     * Click next in the pagination
     */
    @When("^I click next at the top$")
    public void clickNext()
    {
        searchResultsPage.clickNext();
    }

    /**
     * Click previous in the pagination
     */
    @When("^I click previous at the top$")
    public void clickPrevious()
    {
        searchResultsPage.clickPrevious();
    }

    /**
     * Click the plus icon next to a filter
     * <p>
     * @param filterName The filter to click the plus icon next to
     */
    @When("^I click the plus icon next to (.*)$")
    public void clickFilterPlusIcon(String filterName)
    {
        searchResultsPage.clickFilterPlusIcon(filterName);
    }

    /**
     * Click the check-box next to a filter
     * <p>
     * @param filterCheckbox The filter to click the check-box on
     */
    @When("^I click the (.*) checkbox$")
    public void clickFilterCheckbox(String filterName)
    {
        searchResultsPage.clickFilterCheckbox(filterName, scenarioContext);
    }

    /**
     * Check the correct text is shown in the title
     */
    @Then("^I will see the selected industry in the title on the search results page$")
    public void checkTitleText()
    {
        Assert.assertTrue(searchResultsPage.headerTitle().getText().contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.METADATA.INDUSTRY)));
    }

    /**
     * Check the correct keyword is shown in the search query results
     */
    @Then("^I will see the selected keyword in the search results query$")
    public void checkSearchResults()
    {
        searchResultsPage.waitForSearchResults();
        Assert.assertTrue(searchResultsPage.searchResultsHeader().getText().contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.METADATA.KEYWORD)));
    }

    /**
     * Check the piece is added to your cart
     */
    @Then("^the piece will be added to your cart$")
    public void checkAddedToCart()
    {
        searchResultsPage.waitForMessage();
        Assert.assertTrue(searchResultsPage.message().getText().contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.GENERAL.PIECE_NAME)));
    }

    /**
     * Check the number is active
     * <p>
     * @param number The number to check is active or not
     */
    @Then("^(.*) will be active$")
    public void checkNumberActive(String number)
    {
        searchResultsPage.waitForPaginationNavigation();
        Assert.assertTrue(searchResultsPage.paginationNumber(number).getAttribute(ProjectLabel.CLASS).contains(ProjectLabel.ACTIVE));
    }

    /**
     * Check the total number of reports
     */
    @Then("^the total number of reports will be updated$")
    public void checkTotalReports()
    {
        searchResultsPage.waitForPaginationNavigation();
        String actualReportResult = searchResultsPage.totalResults().getText();
        Assert.assertTrue(actualReportResult.substring(actualReportResult.lastIndexOf(" ") + 1).contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.GENERAL.SEARCH_RESULTS)));
    }
}