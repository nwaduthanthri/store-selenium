package com.mintel.stepDefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.mintel.data.Constants.StoreLabel;
import com.mintel.pages.MyAccountPage;
import com.mintel.utilities.ScenarioContext;
import com.mintel.utilities.TestContext;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;

/**
 * Steps relating to the my account feature are found here.
 */
public class MyAccountStepDef
{
    private MyAccountPage myAccountPage;
    private ScenarioContext scenarioContext;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Initialise page classes into objects
     * <p>
     * @param testContext Allows usage of TestContext methods to initialise pages/driver
     */
    public MyAccountStepDef(TestContext testContext)
    {
        myAccountPage = testContext.getPageObjectManager().getMyAccountPage();
        scenarioContext = testContext.getScenarioContext();
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Click on a row within the navigator
     * <p>
     * @param rowName Button name to click
     */
    @When("^I click (.*) on the left$")
    public void clickNavigatorRow(String rowName)
    {
        myAccountPage.clickNavigatorRow(rowName);
    }

    /**
     * Hover over My Account
     */
    @When("^I hover over My Account$")
    public void hoverOverMyAccount()
    {
        myAccountPage.hoverOverMyAccount();
    }

    /**
     * Click text
     * <p>
     * @param textToClick Text to click
     */
    @When("^I click (.*) on text$")
    public void clickText(String textToClick)
    {
        myAccountPage.clickText(textToClick);
    }

    /**
     * Click on a row within the dropdown
     * <p>
     * @param rowName Row to click
     */
    @When("^I click (.*) within the dropdown$")
    public void waitAndClickRow(String rowName)
    {
        myAccountPage.waitAndClickRow(rowName);
    }

    /**
     * Add/Edit the billing address
     */
    @When("^I click Add/Edit under Billing address$")
    public void addBillingAddress()
    {
        myAccountPage.addBillingAddress();
    }

    /**
     * Enter billing address data
     * <p>
     * @param billingAddressData Billing address data to be entered
     */
    @When("^I enter the following Billing address information$")
    public void enterBillingAddressData(DataTable billingAddressData)
    {
        List<Map<String, String>> billingData = billingAddressData.asMaps(String.class, String.class);
        scenarioContext.setContext(ScenarioContext.Context.METADATA.BILLING_DATA, billingData);
        Map<String, String> billingInfo = billingData.get(0);
        myAccountPage.enterBillingAddressData(StoreLabel.FIRST_NAME, billingInfo.get(StoreLabel.FIRST_NAME));
        myAccountPage.enterBillingAddressData(StoreLabel.LAST_NAME, billingInfo.get(StoreLabel.LAST_NAME));
        myAccountPage.enterBillingAddressData(StoreLabel.COMPANY_NAME, billingInfo.get(StoreLabel.COMPANY_NAME));
        myAccountPage.enterBillingAddressData(StoreLabel.STREET_ADDRESS, billingInfo.get(StoreLabel.STREET_ADDRESS));
        myAccountPage.enterBillingAddressData(StoreLabel.TOWN_CITY, billingInfo.get(StoreLabel.TOWN_CITY));
//        myAccountPage.enterBillingAddressData(StoreLabel.STATE_COUNTRY, billingInfo.get(StoreLabel.STATE_COUNTRY));
        myAccountPage.enterBillingAddressData(StoreLabel.POSTCODE, billingInfo.get(StoreLabel.POSTCODE));
        myAccountPage.enterBillingAddressData(StoreLabel.PHONE, billingInfo.get(StoreLabel.PHONE));
        myAccountPage.enterBillingAddressData(StoreLabel.PHONE, billingInfo.get(StoreLabel.PHONE));
    }
    
    /**
     * Select a country within a drop-down
     * <p>
     * @param country The country to select
     */
    @When("^I select (.*) within the Country / Region dropdown$")
    public void selectCountry(String country)
    {
        myAccountPage.selectCountry(country);
    }

    /**
     * Delete existing cards
     */
    @When("^I delete any existing cards$")
    public void deleteExistingCards()
    {
        List<WebElement> deleteButton = new ArrayList<WebElement>();
        try
        {
            deleteButton.add(myAccountPage.buttonName(StoreLabel.DELETE_BUTTON));
            if (!(deleteButton.size() == 0))
            {
                myAccountPage.clickButton(StoreLabel.DELETE_BUTTON);
            }
            Assert.assertTrue(myAccountPage.message().getText().contains("Payment method deleted"));
        }
        catch (NoSuchElementException e)
        {
            System.out.println("No delete button present");
        }
    }

    /**
     * Check user is redirected to the My Account page
     */
    @Then("^I will be taken to My Account$")
    public void checkOnMyAccount()
    {
        myAccountPage.waitForMyAccountPage();
        Assert.assertTrue(myAccountPage.dashboard().getText().contains(StoreLabel.MY_DASHBOARD));
    }

    /**
     * Check user's email address is shown
     */
    @Then("^I will see my email address within the Email address field$")
    public void checkEmail()
    {
        myAccountPage.waitForAccountEmailField();
        Assert.assertTrue(myAccountPage.emailField().getAttribute("value").contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.USERS.EMAIL)));
    }

    /**
     * Check the billing address information
     */
    @Then("^the Billing address will be updated/added$")
    public void checkBillingAddress()
    {
        List<Map<String, String>> billingData = scenarioContext.getScenarioContextAsStringListMap(ScenarioContext.Context.METADATA.BILLING_DATA);
        Map<String, String> billingInfo = billingData.get(0);
        Assert.assertTrue(myAccountPage.billingAddress().getText().contains(billingInfo.get(StoreLabel.FIRST_NAME)));
        Assert.assertTrue(myAccountPage.billingAddress().getText().contains(billingInfo.get(StoreLabel.LAST_NAME)));
        Assert.assertTrue(myAccountPage.billingAddress().getText().contains(billingInfo.get(StoreLabel.COMPANY_NAME)));
        Assert.assertTrue(myAccountPage.billingAddress().getText().contains(billingInfo.get(StoreLabel.STREET_ADDRESS)));
        Assert.assertTrue(myAccountPage.billingAddress().getText().contains(billingInfo.get(StoreLabel.TOWN_CITY)));
        Assert.assertTrue(myAccountPage.billingAddress().getText().contains(billingInfo.get(StoreLabel.STATE_COUNTRY)));
        Assert.assertTrue(myAccountPage.billingAddress().getText().contains(billingInfo.get(StoreLabel.POSTCODE)));
    }

    /**
     * Check payment details information
     */
    @Then("^the Payment details will be present$")
    public void checkPaymentDetails()
    {
        List<Map<String, String>> paymentData = scenarioContext.getScenarioContextAsStringListMap(ScenarioContext.Context.METADATA.PAYMENT_DATA);
        String cardNumber = paymentData.get(0).get(StoreLabel.CARD_NUMBER);
        Assert.assertTrue(myAccountPage.paymentDetails().getText().contains(cardNumber.substring(cardNumber.length() - 4)));
        Assert.assertTrue(myAccountPage.paymentDetails().getText().contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.METADATA.EXPIRY_DATE)));
    }
}