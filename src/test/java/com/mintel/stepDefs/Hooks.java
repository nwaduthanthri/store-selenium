package com.mintel.stepDefs;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.mintel.utilities.TestContext;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * Sets up parameters to be run before and after a test
 */
public class Hooks
{
    private TestContext testContext;

    /**
     * Initialise test context so that the driver is created
     * <p>
     * @param testContext Allows usage of TestContext methods to initialise the driver
     */
    public Hooks(TestContext context)
    {
        testContext = context;
    }

    /**
     * Prepare the steps to be run before each test
     */
    @Before
    public void BeforeSteps()
    {
    }

    /**
     * Checks to see if a scenario fails or not, if it does, it takes a screenshot. Driver is then closed.
     * <p>
     * @param scenario The scenario to check whether it passes or not
     */
    @After
    public void close(Scenario scenario)
    {
        if (scenario.isFailed())
        {
            scenario.embed(((TakesScreenshot) testContext.getDriverUtilities().getDriver()).getScreenshotAs(OutputType.BYTES), "image/png");
//            try
//            {
//                TakesScreenshot ts = (TakesScreenshot) testContext.getDriverUtilities().getDriver();
//                FileUtils.copyFile(ts.getScreenshotAs(OutputType.FILE), new File(System.getProperty("user.dir") + File.separator + "screenshots" + File.separator + scenario.getName() + ".png"));
//            }
//            catch (IOException e)
//            {
//            }
        }
        testContext.getDriverUtilities().cleanUp();
    }
}