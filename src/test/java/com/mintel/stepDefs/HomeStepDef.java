package com.mintel.stepDefs;

import org.junit.Assert;

import com.mintel.data.Constants.ProjectLabel;
import com.mintel.data.Constants.StoreLabel;
import com.mintel.pages.HomePage;
import com.mintel.utilities.ScenarioContext;
import com.mintel.utilities.TestContext;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Steps relating to the home page are found here.
 */
public class HomeStepDef
{
    private HomePage homePage;
    private ScenarioContext scenarioContext;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Initialise page classes into objects
     * <p>
     * @param testContext Allows usage of TestContext methods to initialise pages/driver
     */
    public HomeStepDef(TestContext testContext)
    {
        homePage = testContext.getPageObjectManager().getHomePage();
        scenarioContext = testContext.getScenarioContext();
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Navigate to the Store
     * <p>
     * @param environment The environment to navigate to
     * @throws InterruptedException 
     */
    @Given("^I have navigated to (.*) Store$")
    public void launchUrl(String environment) throws InterruptedException
    {
        homePage.navigateToStore(environment);
        Assert.assertTrue(homePage.getlogo().getText().contentEquals(StoreLabel.MINTEL_STORE));
    }

    /**
     * Click Login/Sign-up in the header
     */
    @When("^I click Login / Signup in the header$")
    public void clickLoginSignUp()
    {
        homePage.clickLoginSignUp();
    }

    /**
     * Select a value within a drop-down
     * <p>
     * @param dropdownName The drop-down name to select within
     * @param dropdownValue The drop-down value to select
     */
    @When("^I (.*) as (.*)$")
    public void selectDropdown(String dropdownName, String dropdownValue)
    {
        homePage.selectDropdown(dropdownName, dropdownValue, scenarioContext);
    }

    /**
     * Click span text
     * <p>
     * @param textToClick Text to click
     */
    @When("^I click (.*) by span$")
    public void clickSpanText(String textToClick)
    {
        homePage.clickSpanText(textToClick);
    }

    /**
     * Enter text into search
     * <p>
     * @param textToEnter Text to enter
     */
    @When("^I enter (.*) into the keyword search$")
    public void enterTextIntoSearch(String textToEnter)
    {
        homePage.enterTextIntoSearch(textToEnter);
        scenarioContext.setContext(ScenarioContext.Context.METADATA.KEYWORD, textToEnter);
    }

    /**
     * Enter text into the top right search
     * <p>
     * @param textToEnter Text to enter 
     */
    @When("^I enter (.*) into the topright keyword search$")
    public void enterTextIntoTopRightSearch(String textToEnter)
    {
        homePage.enterTextIntoTopRightSearch(textToEnter);
        scenarioContext.setContext(ScenarioContext.Context.METADATA.KEYWORD, textToEnter);
    }

    /**
     * Click search in the search bar
     */
    @When("^I click search on the searchbar$")
    public void clickSearch()
    {
        homePage.clickSearch();
    }

    /**
     * Click search in the top right search bar
     */
    @When("^I click search on the topright searchbar$")
    public void clickSearchTopRight()
    {
        homePage.clickSearchTopRight();
    }

    /**
     * Select the first report in the drop-down
     */
    @When("^I select the first report$")
    public void selectFirstReport()
    {
        homePage.selectFirstReport();
    }

    /**
     * Click into a trending report
     * <p>
     * @param rowNumber The row number to determine which report to click into
     */
    @When("^I click into trending report number (.*)$")
    public void clickIntoTrendingReport(String rowNumber)
    {
        homePage.clickIntoTrendingReport(Integer.parseInt(rowNumber), scenarioContext);
    }

    /**
     * Click into a link within the footer
     * <p>
     * @param linkName The link to click
     */
    @When("^I click the (.*) link in the footer$")
    public void clickLink(String linkName)
    {
        homePage.clickLink(linkName);
        scenarioContext.setContext(ScenarioContext.Context.METADATA.INDUSTRY, linkName.replace(ProjectLabel.REPORTS, ProjectLabel.RESEARCH));
    }

    /**
     * Check the correct number is shown
     * <p>
     * @param number The number to check
     */
    @Then("^(.*) will present in the footer$")
    public void checkNumber(String number)
    {
        Assert.assertTrue(homePage.contact().getText().contains(number));
    }
}