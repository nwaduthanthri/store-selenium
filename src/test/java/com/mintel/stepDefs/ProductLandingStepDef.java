package com.mintel.stepDefs;

import org.junit.Assert;

import com.mintel.data.Constants.CURRENCY;
import com.mintel.pages.ProductLandingPage;
import com.mintel.utilities.ScenarioContext;
import com.mintel.utilities.TestContext;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Steps relating to the product landing feature are found here.
 */
public class ProductLandingStepDef
{
    private ProductLandingPage productLandingPage;
    private ScenarioContext scenarioContext;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Initialise page classes into objects
     * <p>
     * @param testContext Allows usage of TestContext methods to initialise pages/driver
     */
    public ProductLandingStepDef(TestContext testContext)
    {
        productLandingPage = testContext.getPageObjectManager().getProductLandingPage();
        scenarioContext = testContext.getScenarioContext();
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Click a row within the table of content
     * <p>
     * @param rowName The row to click
     */
    @When("^I click the (.*) row within the table of contents$")
    public void clickRow(String rowName)
    {
        productLandingPage.clickRowInContent(rowName);
        scenarioContext.setContext(ScenarioContext.Context.GENERAL.TABLE_OF_CONTENT, rowName);
    }

    /**
     * Click currency in the header
     */
    @When("^I click currency in the header$")
    public void clickCurrency()
    {
        productLandingPage.clickCurrencyIcon();
    }

    /**
     * Select a currency
     * <p>
     * @param currency The currency to select
     */
    @When("^I select (.*) from the dropdown$")
    public void selectCurrency(String currency)
    {
        String currencyUnit = "";
        productLandingPage.clickCurrency(currency);
        if (currency.contentEquals(CURRENCY.EUR))
        {
            currencyUnit = CURRENCY.EURO;
        }
        else
        {
            currencyUnit = CURRENCY.DOLLAR;
        }
        scenarioContext.setContext(ScenarioContext.Context.METADATA.CURRENCY, currencyUnit);
    }

    /**
     * Add to card on the product landing page
     */
    @When("^I click Add To Cart on the product landing page$")
    public void addToCart()
    {
        productLandingPage.addToCart(scenarioContext);
    }

    /**
     * Check the correct keyword is shown in the product header
     */
    @Then("^I will see the selected keyword in the title on the product landing page$")
    public void checkProductHeader()
    {
        productLandingPage.waitForProductHeader();
        Assert.assertTrue(productLandingPage.productHeaderTitle().getText().toLowerCase().contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.METADATA.KEYWORD)));
    }

    /**
     * Check the correct row is highlighted
     */
    @Then("^the row will be highlighted$")
    public void checkRowHighlighted()
    {
        String row = scenarioContext.getScenarioContextAsString(ScenarioContext.Context.GENERAL.TABLE_OF_CONTENT);
        productLandingPage.waitForRowToBeHighlighted(row);
        Assert.assertTrue(productLandingPage.tableOfContentRow(row).getAttribute("class").contentEquals("active"));
    }

    /**
     * Check the title is shown in the product landing page
     */
    @Then("^I will see the title of the report on the product landing page$")
    public void checkTitleInHeader()
    {
        productLandingPage.waitForProductHeader();
        for (String word : scenarioContext.getScenarioContextAsStringList(ScenarioContext.Context.GENERAL.PIECE_NAME))
        {
            Assert.assertTrue(productLandingPage.productHeaderTitle().getText().contains(word));
        }
    }

    /**
     * Check the currency of the price
     */
    @Then("^the report price will change$")
    public void checkPriceChange()
    {
        String currency = scenarioContext.getScenarioContextAsString(ScenarioContext.Context.METADATA.CURRENCY);
        productLandingPage.waitForPriceToChange(currency);
        Assert.assertTrue(productLandingPage.productHeaderPrice().getText().contains(currency));
    }
}