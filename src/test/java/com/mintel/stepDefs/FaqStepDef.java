package com.mintel.stepDefs;

import org.junit.Assert;

import com.mintel.pages.FaqPage;
import com.mintel.utilities.ScenarioContext;
import com.mintel.utilities.TestContext;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Steps relating to the FAQ feature are found here.
 */
public class FaqStepDef
{
    private FaqPage faqPage;
    private ScenarioContext scenarioContext;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Initialise page classes into objects
     * <p>
     * @param testContext Allows usage of TestContext methods to initialise pages/driver
     */
    public FaqStepDef(TestContext testContext)
    {
        faqPage = testContext.getPageObjectManager().getFaqPage();
        scenarioContext = testContext.getScenarioContext();
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Click on a section within the FAQ page
     * <p>
     * @param sectionName Section to click
     */
    @When("^I click the (.*) section$")
    public void clickSection(String sectionName)
    {
        faqPage.waitForFAQPage();
        scenarioContext.setContext(ScenarioContext.Context.GENERAL.SECTION_NAME, sectionName);
        faqPage.clickSection(sectionName, scenarioContext);
    }

    /**
     * Check the section is open
     */
    @Then("^it will expand open$")
    public void checkSectionOpen()
    {
        Assert.assertTrue(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.GENERAL.ATTRIBUTE).contentEquals("true"));
    }
}