package com.mintel.stepDefs;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.mintel.data.Constants.StoreLabel;
import com.mintel.pages.CheckoutPage;
import com.mintel.utilities.CommonMethods;
import com.mintel.utilities.ScenarioContext;
import com.mintel.utilities.TestContext;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;

/**
 * Steps relating to the checkout feature are found here.
 */
public class CheckoutStepDef
{
    private CheckoutPage checkoutPage;
    private ScenarioContext scenarioContext;
    private WebDriver driver;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Initialise page classes into objects
     * <p>
     * @param testContext Allows usage of TestContext methods to initialise pages/driver
     */
    public CheckoutStepDef(TestContext testContext)
    {
        checkoutPage = testContext.getPageObjectManager().getCheckoutPage();
        driver = testContext.getDriverUtilities().getDriver();
        scenarioContext = testContext.getScenarioContext();
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Click the checkout button
     */
    @When("^I click Proceed to checkout$")
    public void clickProceedToCheckout()
    {
        checkoutPage.waitForCheckoutButton();
        checkoutPage.clickCheckoutButton();
    }

    /**
     * Enter mandatory billing data
     * <p>
     * @param mandatoryBillingData Billing data to be entered
     */
    @When("^I enter the following mandatory Billing address information$")
    public void enterMandatoryBillingData(DataTable mandatoryBillingData)
    {
        checkoutPage.waitForSpinner();
        List<Map<String, String>> billingData = mandatoryBillingData.asMaps(String.class, String.class);
        scenarioContext.setContext(ScenarioContext.Context.METADATA.BILLING_DATA, billingData);
        Map<String, String> billingInfo = billingData.get(0);
        checkoutPage.enterBillingData(StoreLabel.FIRST_NAME, billingInfo.get(StoreLabel.FIRST_NAME));
        checkoutPage.enterBillingData(StoreLabel.LAST_NAME, billingInfo.get(StoreLabel.LAST_NAME));
        checkoutPage.enterBillingData(StoreLabel.TELEPHONE, billingInfo.get(StoreLabel.TELEPHONE));
        checkoutPage.enterBillingData(StoreLabel.HOUSE_NUMBER, billingInfo.get(StoreLabel.HOUSE_NUMBER));
        checkoutPage.enterBillingData(StoreLabel.TOWN_CITY, billingInfo.get(StoreLabel.TOWN_CITY));
        checkoutPage.enterBillingData(StoreLabel.POSTCODE, billingInfo.get(StoreLabel.POSTCODE));
        checkoutPage.enterBillingData(StoreLabel.COMPANY_NAME, billingInfo.get(StoreLabel.COMPANY_NAME));
    }

    /**
     * Enter mandatory payment data
     * <p>
     * @param mandatoryPaymentData Payment data to be entered
     */
    @When("^I enter the following mandatory credit card information$")
    public void enterMandatoryPaymentData(DataTable mandatoryPaymentData)
    {
        List<Map<String, String>> paymentData = mandatoryPaymentData.asMaps(String.class, String.class);
        Map<String, String> paymentInfo = paymentData.get(0);
        driver.switchTo().frame(0);
        checkoutPage.enterPaymentData(StoreLabel.TEST_FIELD_CARD_DATA, paymentInfo.get(StoreLabel.CARD_NUMBER));
        driver.switchTo().defaultContent();
        driver.switchTo().frame(1);
        String futureDate = CommonMethods.getFutureDate();
        checkoutPage.enterPaymentData(StoreLabel.MMYY, futureDate);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(2);
        checkoutPage.enterPaymentData(StoreLabel.CVC, paymentInfo.get(StoreLabel.CVC));
        driver.switchTo().defaultContent();
        scenarioContext.setContext(ScenarioContext.Context.METADATA.PAYMENT_METHOD, StoreLabel.CREDIT_CARD);
        scenarioContext.setContext(ScenarioContext.Context.METADATA.PAYMENT_DATA, paymentData);
        scenarioContext.setContext(ScenarioContext.Context.METADATA.EXPIRY_DATE, futureDate);
    }

    /**
     * Click a radio button
     * <p>
     * @param radioButton Radio button to click 
     */
    @When("^I click the (.*) radio icon$")
    public void clickRadioButton(String radioButton)
    {
        scenarioContext.setContext(ScenarioContext.Context.METADATA.PAYMENT_METHOD, radioButton);
        checkoutPage.clickRadioButton(radioButton);
    }

    /**
     * Check conditions
     */
    @When("^I check the commercial terms and conditions$")
    public void checkConditions()
    {
        checkoutPage.checkConditions();
    }

    /**
     * Remove an item from the cart
     */
    @When("^I remove the item from the cart$")
    public void removeItem()
    {
        checkoutPage.removeItem(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.GENERAL.PIECE_NAME));
    }

    /**
     * Check the selected piece is shown in the order summary
     */
    @Then("^the selected piece will be shown in the Order summary$")
    public void checkPieceShown()
    {
        checkoutPage.waitForShopTable();
        Assert.assertTrue(checkoutPage.shopTable().getText().contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.GENERAL.PIECE_NAME)));
    }

    /**
     * Check the correct payment is used
     */
    @Then("^the payment method will be mentioned$")
    public void checkPaymentMethod()
    {
        checkoutPage.waitForThankYouOrder();
        Assert.assertTrue(checkoutPage.thankYouOrder().getText().contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.METADATA.PAYMENT_METHOD)));
    }

    /**
     * Check the billing address information
     */
    @Then("^the Billing address information will be displayed$")
    public void checkBillingInfo()
    {
        List<Map<String, String>> billingData = scenarioContext.getScenarioContextAsStringListMap(ScenarioContext.Context.METADATA.BILLING_DATA);
        Map<String, String> billingInfo = billingData.get(0);
        Assert.assertTrue(checkoutPage.customerDetails().getText().contains(billingInfo.get(StoreLabel.FIRST_NAME)));
        Assert.assertTrue(checkoutPage.customerDetails().getText().contains(billingInfo.get(StoreLabel.LAST_NAME)));
        Assert.assertTrue(checkoutPage.customerDetails().getText().contains(billingInfo.get(StoreLabel.TELEPHONE)));
        Assert.assertTrue(checkoutPage.customerDetails().getText().contains(billingInfo.get(StoreLabel.HOUSE_NUMBER)));
        Assert.assertTrue(checkoutPage.customerDetails().getText().contains(billingInfo.get(StoreLabel.TOWN_CITY)));
        Assert.assertTrue(checkoutPage.customerDetails().getText().contains(billingInfo.get(StoreLabel.POSTCODE)));
        Assert.assertTrue(checkoutPage.customerDetails().getText().contains(billingInfo.get(StoreLabel.COMPANY_NAME)));
        Assert.assertTrue(checkoutPage.customerDetails().getText().contains(scenarioContext.getScenarioContextAsString(ScenarioContext.Context.USERS.EMAIL)));
    }

    /**
     * Check the sub-totals are correct
     */
    @Then("^the subtotals for the carts will be correct$")
    public void checkSubtotals()
    {
        for (Entry<String, String> entry : scenarioContext.getScenarioContextAsStringMap(ScenarioContext.Context.GENERAL.PIECE_PRICE).entrySet())
        {
            Assert.assertTrue(checkoutPage.getPrice(entry.getKey()).contentEquals(entry.getValue()));
        }
    }

    /**
     * Check the grand total is correct
     */
    @Then("^the grand total will be correct$")
    public void checkGrandTotal()
    {
        List<Integer> prices = new ArrayList<Integer>();
        for (String value : scenarioContext.getScenarioContextAsStringMap(ScenarioContext.Context.GENERAL.PIECE_PRICE).values())
        {
            prices.add((int) Double.parseDouble(value.replaceAll("[£,$€ ]", "")));
        }
        int total = prices.stream().mapToInt(Integer::intValue).sum();
        Assert.assertTrue(checkoutPage.getGrandTotal() == total);
    }

    /**
     * Check not able to download
     */
    @Then("^the option to download will not be present$")
    public void checkNotAbleToDownload()
    {
        Assert.assertTrue(checkoutPage.downloadOption().size() == 0);
    }

    /**
     * Check able to download
     */
    @Then("^the option to download will be present$")
    public void checkAbleToDownload()
    {
        try
        {
            Assert.assertTrue(checkoutPage.downloadOption().get(0).isDisplayed());
        }
        catch (NoSuchElementException e)
        {
            fail();
        }
    }

    /**
     * Check an error message is displayed
     * <p>
     * @param errorMessages Error messages to check for
     */
    @Then("^the following error message will be displayed$")
    public void checkErrorMessage(DataTable errorMessages)
    {
        for (String errorMessage : errorMessages.asList())
        {
            Assert.assertTrue(checkoutPage.errorMessageContainer().getText().contains(errorMessage));
        }
    }
}