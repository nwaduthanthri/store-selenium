package com.mintel.stepDefs;

import org.junit.Assert;

import com.mintel.data.Constants.StoreLabel;
import com.mintel.data.TestData.Email;
import com.mintel.pages.LoginPage;
import com.mintel.utilities.ScenarioContext;
import com.mintel.utilities.TestContext;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Steps relating to logging in are found here.
 */
public class LoginStepDef
{
    private LoginPage loginPage;
    private ScenarioContext scenarioContext;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Initialise page classes into objects
     * <p>
     * @param testContext Allows usage of TestContext methods to initialise pages/driver
     */
    public LoginStepDef(TestContext testContext)
    {
        loginPage = testContext.getPageObjectManager().getLoginPage();
        scenarioContext = testContext.getScenarioContext();
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Enter a value into a field
     * <p>
     * @param fieldName The field to enter the value into
     */
    @When("^I enter a value into the (.*) field$")
    public void enterValue(String fieldName)
    {
        switch (fieldName)
        {
            case StoreLabel.EMAIL_ADDRESS:
                String email = loginPage.generateEmail();
                scenarioContext.setContext(ScenarioContext.Context.USERS.EMAIL, email);
                loginPage.enterRegistrationEmail(email);
                break;
            case StoreLabel.PASSWORD:
                loginPage.enterPassword(loginPage.generatePassword());
                break;
            case StoreLabel.USERNAME_EMAIL_ADDRESS:
                loginPage.enterUsernameEmail(Email.MINTEL_USER);
                break;
            default:
                loginPage.enterPassword(loginPage.generatePassword());
        }
    }

    /**
     * Click button
     * <p>
     * @param buttonName Button name to click
     */
    @When("^I click the (.*) button$")
    public void clickButton(String buttonName)
    {
        loginPage.clickButton(buttonName);
    }

    /**
     * Click the forgotten password link
     */
    @When("^I click the Lost your password link$")
    public void clickForgottenPasswordLink()
    {
        loginPage.clickForgottenPasswordLink();
    }

    /**
     * Login using test data
     */
    @When("^I login using test credentials$")
    public void loginUsingTestData()
    {
        loginPage.enterUsernameEmail(Email.TEST_CREDENTIALS);
        loginPage.enterPassword(Email.TEST_CREDENTIALS);
    }

    /**
     * Check user is on the login screen
     */
    @Then("^I will be taken to the login screen$")
    public void checkOnLoginScreen()
    {
        loginPage.waitForLoginPage();
        Assert.assertTrue(loginPage.column2().getText().contains(StoreLabel.REGISTER));
    }

    /**
     * Check user is logged out
     */
    @Then("^I will be logged out$")
    public void checkUserLoggedOut()
    {
        Assert.assertTrue(loginPage.getLoginSignup().getText().contains(StoreLabel.LOGIN_SIGNUP));
    }

    /**
     * Check button changes
     * <p>
     * @param buttonName Button to check for
     */
    @Then("^the button will change to (.*)$")
    public void checkButtonChanges(String buttonName)
    {
        loginPage.waitForButton(buttonName);
    }

    /**
     * Check the correct message is shown
     * <p>
     * @param message Message to check for
     */
    @Then("^I will get the (.*) message$")
    public void checkMessage(String message)
    {
        Assert.assertTrue(loginPage.message().getText().contains(message));
    }
    
    /**
     * Wait for the spinner to complete
     */
    @Then("^the spinner will disappear$")
    public void waitForSpinnerToLoad()
    {
        loginPage.waitForSpinner();
    }
}