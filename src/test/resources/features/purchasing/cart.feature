@regression @purchasing @cart
Feature: Test the cart features

  Background: Set test pre conditions
    Given I have navigated to Live Store
    
  Scenario: Test able to delete an item from your cart
    When I Select Industry as Drinks
    And I Select Market as Germany
    And I click the Search button
    Then I will see the selected industry in the title on the search results page
    When I click Add To Cart for Market Sizes
    Then the piece will be added to your cart
    When I click View cart within the message
    And I remove the item from the cart
    Then I will get the removed message
    

  Scenario: Test the subtotal and grand total prices are correct
    When I Select Industry as Drinks
    And I Select Market as China
    And I click the Search button
    Then I will see the selected industry in the title on the search results page
    When I click Add To Cart for Market Sizes
    Then the piece will be added to your cart
    When I click Add To Cart for 2022
    And I click View cart within the message
		Then the subtotals for the carts will be correct
		And the grand total will be correct