@regression @purchasing @creditCard
Feature: Test purchasing through a credit card

  Background: Set test pre conditions
    Given I have navigated to Beta Store
    
  Scenario: Test able to purchase a report from the industry search via credit card
    When I Select Industry as Drinks
    And I Select Market as Germany
    And I click the Search button
    Then I will see the selected industry in the title on the search results page
    When I click Add To Cart for Market Sizes
    Then the piece will be added to your cart
    When I click View cart within the message
    And I click Proceed to checkout
    Then the selected piece will be shown in the Order summary
    When I enter the following mandatory Billing address information
      | First name | Last name | Telephone | House number and street name | Town / City | Postcode / ZIP | Company name |
      | Greg       | Gregface  | 020428572 | 72 fake street               | London      | W4 1QA         | Mintel       |
    And I enter a value into the Email address field
    And I enter a value into the Password field
    And I enter the following mandatory credit card information
      | Card Number      | CVC | 
      | 4242424242424242 | 123 |   
    And I check the commercial terms and conditions
    And I click the Complete Payment button
    Then the payment method will be mentioned
    And the Billing address information will be displayed
    And the option to download will be present
    
    
  Scenario: Test an error message is shown when the password field is left blank
    When I Select Industry as Drinks
    And I Select Market as Germany
    And I click the Search button
    Then I will see the selected industry in the title on the search results page
    When I click Add To Cart for Market Sizes
    Then the piece will be added to your cart
    When I click View cart within the message
    And I click Proceed to checkout
    Then the selected piece will be shown in the Order summary
    When I enter the following mandatory Billing address information
      | First name | Last name | Telephone | House number and street name | Town / City | Postcode / ZIP | Company name |
      | Greg       | Gregface  | 020428572 | 72 fake street               | London      | W4 1QA         | Mintel       |
    And I enter a value into the Email address field
    And I enter the following mandatory credit card information
      | Card Number      | CVC | 
      | 4242424242424242 | 123 |   
    And I check the commercial terms and conditions
    And I click the Complete Payment button
    Then the following error message will be displayed
    	| Create account password |