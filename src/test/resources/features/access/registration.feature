@regression @access @registration
Feature: Testing registration functionality

  Scenario: Test able to register to the store and then logout successfully
    Given I have navigated to Live Store
    When I click Login / Signup in the header
    Then I will be taken to the login screen
    When I enter a value into the Email address field
    And I enter a value into the Password field
    And I click the Register button
    Then I will be taken to My Account
    When I click Account details on the left
    Then I will see my email address within the Email address field
    When I hover over My Account
    And I click Log Out within the dropdown
    Then I will be taken to My Account
    When I click Confirm and log out on text
    Then I will be taken to the login screen
    And I will be logged out    