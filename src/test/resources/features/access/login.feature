@regression @access @login
Feature: Testing login functionality

  Background: Set test pre conditions
    Given I have navigated to Live Store
    And I click Login / Signup in the header
    And I will be taken to the login screen
    
    
  Scenario: Test the login button changes for a mintel user
    When I enter a value into the Username or email address field
		Then the button will change to Log in with Mintel portal
		
		
  Scenario: Test the forgot password option
    When I click the Lost your password link
    And I enter a value into the Username or email address field
    And I click the Reset password button
    Then I will get the Password reset email has been sent message