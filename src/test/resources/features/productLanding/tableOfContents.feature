@regression @productLanding @tableOfContents
Feature: Testing the table of contents

  Scenario Outline: Test clicking rows on the table of content takes the user to the correct part of the page
    Given I have navigated to Live Store
    When I click search by keywords by span
    And I enter wine into the keyword search
    And I select the first report
    And I click the <Row Name> row within the table of contents
    Then the row will be highlighted
    
    Examples: 
      | Row Name          |
      | Description       |
      | Table of contents |