@regression @productLanding @currency
Feature: Testing the different currencies

  Background: Set test pre conditions
    Given I have navigated to Live Store
    And I click search by keywords by span
    And I enter wine into the keyword search
    And I select the first report
    And I will see the selected keyword in the title on the product landing page
    

  Scenario: Test the price changes when a different currency is selected
    When I click currency in the header
    And I select EUR from the dropdown
    Then the report price will change
    

  Scenario: Test a report with currency EUR can be added to the cart
    When I click currency in the header
    And I select EUR from the dropdown
    Then the report price will change
    When I click Add To Cart on the product landing page
    Then the piece will be added to your cart
    When I click View cart within the message
    Then the subtotals for the carts will be correct