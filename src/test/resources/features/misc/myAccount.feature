@regression @misc @myAccount
Feature: Testing My Account functionality

  Background: Set test pre conditions
    Given I have navigated to Live Store
    And I click Login / Signup in the header
    And I will be taken to the login screen
    And I login using test credentials
    And I click the Log in button
    And I will be taken to My Account
    

  Scenario: Test able to edit your address
    When I click Address on the left
    And I click Add/Edit under Billing address
    And I enter the following Billing address information
      | First name | Last name | Company name | Street address | Town / City | State / County | Postcode / ZIP | Phone     |
      | Greg       | Gregface  | Mintel       | 72 fake street | London      | London         | W4 1QA         | 020428572 |
    And I select United Kingdom within the Country / Region dropdown
    And I click the Save address button
    Then I will get the Address changed successfully message
    And the Billing address will be updated/added
    
    
  Scenario: Test able to add card details
    When I click Payment methods on the left
    And I delete any existing cards
    And I click the Add new card button
    And I enter the following mandatory credit card information
      | Card Number      | CVC | 
      | 4242424242424242 | 123 |  
    And I click the Save Card button
    Then the spinner will disappear
    And I will get the Payment method successfully added message
    And the Payment details will be present