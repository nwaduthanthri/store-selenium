@regression @misc @faq
Feature: Testing the FAQ page

  
  Scenario: Test the FAQ opens up correctly
    Given I have navigated to Live Store
    When I click the FAQ link in the footer
    And I click the HOW DO I PLACE AN ORDER? section
    Then it will expand open