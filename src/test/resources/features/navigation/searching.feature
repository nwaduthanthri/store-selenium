@regression @navigation @searching
Feature: Testing searching functionality

  Background: Set test pre conditions
    Given I have navigated to Live Store
    
    
  Scenario: Search by filters
    When I Select Industry as Drinks
    And I Select Market as Germany
    And I click the Search button
    Then I will see the selected industry in the title on the search results page
    

  Scenario: Search by keywords and select first entry
    When I click search by keywords by span
    And I enter cheese into the keyword search
    And I select the first report
    Then I will see the selected keyword in the title on the product landing page
    
    
  Scenario: Search by keywords and click search
    When I click search by keywords by span
    And I enter cheese into the keyword search
    And I click search on the searchbar
    Then I will see the selected keyword in the search results query
    
 
  Scenario: Search by keywords in the top right search bar
    When I enter coffee into the topright keyword search
    And I click search on the topright searchbar
    Then I will see the selected keyword in the search results query
    

  Scenario: Test the left-hand search filters to further refine searches
    When I Select Industry as Electrical Goods
    And I Select Market as Germany
    And I click the Search button
    Then I will see the selected industry in the title on the search results page
    When I click the plus icon next to Automotive
    And I click the Accessories and Maintenance checkbox
    Then the total number of reports will be updated