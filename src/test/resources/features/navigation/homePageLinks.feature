@regression @navigation @homePageLinks
Feature: Test links within the home page

  Background: Set test pre conditions
    Given I have navigated to Live Store
    
    
  Scenario: Test able to click into a trending report
    When I click into trending report number 0
    Then I will see the title of the report on the product landing page
    
    
  Scenario: Test footer links redirects you to the right search results
    When I click the US Market Reports link in the footer
    Then I will see the selected industry in the title on the search results page