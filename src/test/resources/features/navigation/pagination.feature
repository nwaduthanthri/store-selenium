@regression @navigation @pagination
Feature: Testing the pagination functionality

  Scenario: Search by filters
    Given I have navigated to Live Store
    When I Select Industry as Drinks
    And I Select Market as Germany
    And I click the Search button
    Then I will see the selected industry in the title on the search results page
    When I click next at the top
    Then 2 will be active
    When I click previous at the top
    Then 1 will be active