package com.mintel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mintel.utilities.CommonMethods;

/**
 * Common elements and Methods that use WebElements used across this project will be stored here
 */
public class BasePage
{
    protected WebDriver driver;
    protected Actions action;
    protected WebDriverWait wait;
    protected WebDriverWait waitLong;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Set up initElements to enable usage of page object model
     * <p>
     * @param driver The driver to set within a page
     */
    public BasePage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        action = new Actions(driver);
        wait = new WebDriverWait(driver, 30);
        waitLong = new WebDriverWait(driver, 300);
        waitLong.ignoring(NoSuchElementException.class);
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Mintel logo found in the header
     * <p>
     * @return logo Return the logo WebElement
     */
    public WebElement getlogo()
    {
        return driver.findElement(By.id("site-header")).findElement(By.cssSelector(".header-titles .site-logo"));
    }

    /**
     * Login/Sign-up option found in the header
     * <p>
     * @return login/signup Return the login/signup WebElement
     */
    public WebElement getLoginSignup()
    {
        return driver.findElement(By.cssSelector(".login a"));
    }

    /**
     * Message element
     * <p>
     * @return message Return the Message WebElement
     */
    public WebElement message()
    {
        return driver.findElement(By.cssSelector(".woocommerce-message"));
    }

    /**
     * Button element found on the login screen
     * <p>
     * @param buttonName The button name to look for
     * @return buttonName Return the button WebElement
     */
    public WebElement buttonName(String buttonName)
    {
        try
        {
            return driver.findElement(By.xpath("//button[text()='" + buttonName + "']"));
        }
        catch (NoSuchElementException e)
        {
            return driver.findElement(By.xpath("//a[text()='" + buttonName + "']"));
        }
    }

    /**
     * Wait for the spinner to load
     */
    public void waitForSpinner()
    {
        try
        {
            wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.cssSelector(".blockUI.blockOverlay"))));
        }
        catch (NoSuchElementException e)
        {
        }
    }

    /**
     * Click Login/Sign-up in the header
     */
    public void clickLoginSignUp()
    {
        getLoginSignup().click();
    }

    /**
     * Click currency in the header
     */
    public void clickCurrencyIcon()
    {
        driver.findElement(By.cssSelector(".currency")).click();
    }

    /**
     * Click a currency
     * <p>
     * @param currency The currency to click
     */
    public void clickCurrency(String currency)
    {
        driver.findElement(By.cssSelector(".select2-results__options li")).findElement(By.xpath("//span[contains(text(),'" + currency + "')]")).click();
    }

    /**
     * Click button
     * <p>
     * @param buttonName Button name to click
     */
    public void clickButton(String buttonName)
    {
        CommonMethods.scrollElementIntoView(driver, buttonName(buttonName));
        CommonMethods.clickElement(driver, buttonName(buttonName));
    }
}