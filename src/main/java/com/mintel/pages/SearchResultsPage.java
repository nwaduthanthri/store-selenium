package com.mintel.pages;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.mintel.data.Constants.ProjectLabel;
import com.mintel.data.Constants.StoreLabel;
import com.mintel.utilities.CommonMethods;
import com.mintel.utilities.ScenarioContext;

/**
 * This page contains methods and WebElements relating to the search results Page
 */
public class SearchResultsPage extends BasePage
{
    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Use the constructor set within Base Page
     * <p>
     * @param driver The driver to set within the page
     */
    public SearchResultsPage(WebDriver driver)
    {
        super(driver);
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Header title
     * <p>
     * @return headerTitle Return the header title WebElement
     */
    public WebElement headerTitle()
    {
        return driver.findElement(By.cssSelector(".term-description h1"));
    }

    /**
     * Search results query header
     * <p>
     * @return searchResultsHeader Return the header title WebElement
     */
    public WebElement searchResultsHeader()
    {
        return driver.findElement(By.cssSelector(".search-result-query #search_value"));
    }

    /**
     * Pagination number
     * <p>
     * @param number The number to determine the element
     * @return paginationNumber Return the pagination number title WebElement
     */
    public WebElement paginationNumber(String number)
    {
        return driver.findElement(By.cssSelector(".pagination")).findElement(By.xpath("//a[text()='" + number + "']"));
    }

    /**
     * Total number of results
     * <p>
     * @return totalResults Return the total number of results
     */
    public WebElement totalResults()
    {
        return driver.findElement(By.cssSelector(".ms-shop-loop-header .results"));
    }

    /**
     * Wait for the search results header to load
     */
    public void waitForSearchResults()
    {
        wait.until(ExpectedConditions.visibilityOf(searchResultsHeader()));
    }

    /**
     * Click a button on a piece
     * <p>
     * @param buttonName Button name to click
     * @param pieceType Type of piece to look within
     * @param scenarioContext Share test data
     */
    public void clickButtonInSearchList(String buttonName, String pieceType, ScenarioContext scenarioContext)
    {
        for (WebElement rowName : driver.findElements(By.cssSelector(".products li")))
        {
            if (rowName.getText().contains(pieceType))
            {
                CommonMethods.scrollElementIntoView(driver, rowName.findElement(By.cssSelector(("a h2"))));
                String title = rowName.findElement(By.cssSelector(("a h2"))).getText();
                scenarioContext.setContext(ScenarioContext.Context.GENERAL.PIECE_NAME, title);
                Map<String, String> piecePriceMap = new HashMap<String, String>();
                piecePriceMap.put(title, rowName.findElement(By.cssSelector((".price bdi"))).getText());
                scenarioContext.setOrUpdateContextMap(ScenarioContext.Context.GENERAL.PIECE_PRICE, piecePriceMap);
                if (buttonName.contentEquals(StoreLabel.ADD_TO_CART))
                {
                    WebElement addToCartButton = rowName.findElement(By.cssSelector("a[class*='product_type_simple']"));
                    CommonMethods.clickElement(driver, addToCartButton);
                    wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(addToCartButton, "class", "loading")));
                    break;
                }
                else
                {
                    CommonMethods.clickElement(driver, rowName.findElement(By.cssSelector("a[class*='find']")));
                    break;
                }
            }
        }
    }

    /**
     * Wait for the message to appear
     */
    public void waitForMessage()
    {
        wait.until(ExpectedConditions.visibilityOf(message()));
    }

    /**
     * Click the view cart button within the message
     */
    public void clickViewCart()
    {
        CommonMethods.clickElement(driver, message().findElement(By.cssSelector(".button")));
    }

    /**
     * Click next in the pagination
     */
    public void clickNext()
    {
        CommonMethods.clickElement(driver, driver.findElement(By.cssSelector(".pagination .next")));
    }

    /**
     * Click previous in the pagination
     */
    public void clickPrevious()
    {
        CommonMethods.clickElement(driver, driver.findElement(By.cssSelector(".pagination .prev")));
    }

    /**
     * Wait for pagination navigation to load
     */
    public void waitForPaginationNavigation()
    {
        wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(driver.findElement(By.cssSelector("div[data-name='page_nav_facet']")), ProjectLabel.CLASS, ProjectLabel.LOADING)));
    }

    /**
     * Click the plus icon next to a filter
     * <p>
     * @param filterName The filter to click the plus icon next to
     */
    public void clickFilterPlusIcon(String filterName)
    {
        WebElement filterPlus = getFilter(filterName).findElement(By.className("facetwp-expand"));
        CommonMethods.scrollElementIntoView(driver, filterPlus);
        CommonMethods.clickElement(driver, filterPlus);
    }

    /**
     * Click the check-box next to a filter
     * <p>
     * @param filterCheckbox The filter to click the check-box on
     * @param scenarioContext Share test data
     */
    public void clickFilterCheckbox(String filterName, ScenarioContext scenarioContext)
    {
        CommonMethods.scrollElementIntoView(driver, getFilter(filterName));
        CommonMethods.clickElement(driver, getFilter(filterName));
        scenarioContext.setContext(ScenarioContext.Context.GENERAL.SEARCH_RESULTS, getFilter(filterName).findElement(By.className("facetwp-counter")).getText().replaceAll("[()]", ""));
    }

    /* **************** 
     *  PRIVATE METHODS 
     ****************** */

    /**
     * Get the filter WebElement
     * <p>
     * @param filterName The filter to return
     */
    private WebElement getFilter(String filterName)
    {
        return driver.findElement(By.cssSelector(".ms-left-sidebar-filters")).findElement(By.xpath("//div[contains(text(),'" + filterName + "')]"));
    }
}