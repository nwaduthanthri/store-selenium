package com.mintel.pages;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.mintel.utilities.CommonMethods;
import com.mintel.utilities.ScenarioContext;

/**
 * This page contains methods and WebElements relating to the product landing Page
 */
public class ProductLandingPage extends BasePage
{
    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Use the constructor set within Base Page
     * <p>
     * @param driver The driver to set within the page
     */
    public ProductLandingPage(WebDriver driver)
    {
        super(driver);
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Product Header title
     * <p>
     * @return productHeaderTitle Return the header title WebElement
     */
    public WebElement productHeaderTitle()
    {
        return driver.findElement(By.cssSelector(".entry-summary-inner h1"));
    }

    /**
     * Table of contents rows
     * <p>
     * @param rowName The row to get
     * @return productHeaderTitle Return the row WebElement
     */
    public WebElement tableOfContentRow(String rowName)
    {
        return driver.findElement(By.cssSelector(".content-links")).findElement(By.xpath("//a[text()='" + rowName + "']"));
    }

    /**
     * Price in the title
     * <p>
     * @return price Return the price WebElement in the title
     */
    public WebElement productHeaderPrice()
    {
        return driver.findElement(By.cssSelector(".entry-summary-inner .price"));
    }

    /**
     * Wait for the header to load
     */
    public void waitForProductHeader()
    {
        wait.until(ExpectedConditions.visibilityOf(productHeaderTitle()));
    }

    /**
     * Wait for a row within the table of content to be highlighted
     * <p>
     * @param rowName The row to wait for
     */
    public void waitForRowToBeHighlighted(String rowName)
    {
        wait.until(ExpectedConditions.attributeToBe(tableOfContentRow(rowName), "class", "active"));
    }

    /**
     * Click a row within the table of content
     * <p>
     * @param rowName The row to click
     */
    public void clickRowInContent(String rowName)
    {
        CommonMethods.clickElement(driver, tableOfContentRow(rowName));
    }

    /**
     * Wait for the price to change
     * <p>
     * @param currency The currency to wait for
     */
    public void waitForPriceToChange(String currency)
    {
        wait.until(ExpectedConditions.textToBePresentInElement(productHeaderPrice(), currency));
    }

    /**
     * Add to card on the product landing page
     * <p>
     * @param scenarioContext Share test data
     */
    public void addToCart(ScenarioContext scenarioContext)
    {
        String title = productHeaderTitle().getText();
        Map<String, String> piecePriceMap = new HashMap<String, String>();
        scenarioContext.setContext(ScenarioContext.Context.GENERAL.PIECE_NAME, title);
        piecePriceMap.put(title, productHeaderPrice().findElement(By.cssSelector(("bdi"))).getText());
        scenarioContext.setOrUpdateContextMap(ScenarioContext.Context.GENERAL.PIECE_PRICE, piecePriceMap);
        CommonMethods.clickElement(driver, driver.findElement(By.cssSelector(".cart button[name='add-to-cart']")));
    }
}