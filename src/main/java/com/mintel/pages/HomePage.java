package com.mintel.pages;

import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.mintel.data.Constants.Messages;
import com.mintel.data.Constants.ProjectLabel;
import com.mintel.data.Constants.StoreLabel;
import com.mintel.data.Constants.Url;
import com.mintel.utilities.CommonMethods;
import com.mintel.utilities.ScenarioContext;

/**
 * This page contains methods and WebElements relating to the store Home Page
 */
public class HomePage extends BasePage
{
    /**
     * Main search bar
     */
    @FindBy(css = "input[id='homepage-keyword-search']")
    private WebElement mainSearchBar;

    /**
     * Top right search bar field
     */
    @FindBy(css = "input[id='st-search-input-sidebar']")
    private WebElement topRightSearchBar;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Use the constructor set within Base Page
     * <p>
     * @param driver The driver to set within the page
     */
    public HomePage(WebDriver driver)
    {
        super(driver);
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Contact WebElement
     * <p>
     * @return contact Return the contact info webelement
     */
    public WebElement contact()
    {
        return driver.findElement(By.cssSelector(".contact"));
    }

    /**
     * Navigates the correct URL
     * <p>
     * @param environment Environment to navigate to
     */
    public void navigateToStore(String environment)
    {
        switch (environment)
        {
            case ProjectLabel.BETA:
                driver.get(Url.BETA);
                break;
            case ProjectLabel.LIVE:
                driver.get(Url.LIVE);
                break;
            default:
                fail(Messages.NO_ENVIRONMENT);
                break;
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    /**
     * Select a value within a drop-down
     * <p>
     * @param dropdownName The drop-down name to select within
     * @param dropdownValue The drop-down value to select
     * @param scenarioContext Share test data
     */
    public void selectDropdown(String dropdownName, String dropdownValue, ScenarioContext scenarioContext)
    {
        if (dropdownName.contentEquals(StoreLabel.SELECT_INDUSTRY))
        {
            CommonMethods.selectByVisibleText(driver.findElement(By.id("industryField")), dropdownValue);
            scenarioContext.setContext(ScenarioContext.Context.METADATA.INDUSTRY, dropdownValue);
        }
        else
        {
            CommonMethods.selectByVisibleText(driver.findElement(By.id("countryField")), dropdownValue);
        }
    }

    /**
     * Click span text
     * <p>
     * @param textToClick Text to click
     */
    public void clickSpanText(String textToClick)
    {
        driver.findElement(By.xpath("//span[text()='" + textToClick + "']")).click();
    }

    /**
     * Enter text into search
     * <p>
     * @param textToEnter Text to enter
     */
    public void enterTextIntoSearch(String textToEnter)
    {
        action.moveToElement(mainSearchBar).click().sendKeys(textToEnter).perform();
    }

    /**
     * Enter text into the top right search
     * <p>
     * @param textToEnter Text to enter
     */
    public void enterTextIntoTopRightSearch(String textToEnter)
    {
        action.moveToElement(topRightSearchBar).click().sendKeys(textToEnter).perform();
    }

    /**
     * Click search in the search bar
     */
    public void clickSearch()
    {
        mainSearchBar.findElement(By.xpath("..")).findElement(By.id("link-sidebar")).click();
    }

    /**
     * Click search in the top right search bar
     */
    public void clickSearchTopRight()
    {
        topRightSearchBar.findElement(By.xpath("..")).findElement(By.id("link-sidebar")).click();
    }

    /**
     * Select the first report in the drop-down
     */
    public void selectFirstReport()
    {
        driver.findElement(By.cssSelector(".autocomplete ul li")).click();
    }

    /**
     * Click into a trending report
     * <p>
     * @param rowNumber The row number to determine which report to click into
     * @param scenarioContext Share test data
     */
    public void clickIntoTrendingReport(int rowNumber, ScenarioContext scenarioContext)
    {
        WebElement trendingReport = driver.findElements(By.cssSelector(".trending-list-wrap li")).get(rowNumber);
        CommonMethods.scrollElementIntoView(driver, trendingReport);
        String[] words = trendingReport.findElement(By.cssSelector(".report-teaser h3")).getText().split("\\s");
        for (int i = 0; i < words.length; i++)
        {
            words[i] = words[i].replace(".", "");
        }
        scenarioContext.setContext(ScenarioContext.Context.GENERAL.PIECE_NAME, Arrays.asList(words));
        CommonMethods.clickElement(driver, trendingReport.findElement(By.cssSelector(".report-teaser .more-link")));
    }

    /**
     * Click into a link within the footer
     * <p>
     * @param linkName The link to click
     */
    public void clickLink(String linkName)
    {
        WebElement link = driver.findElement(By.cssSelector(".footer-widgets a[title='" + linkName + "']"));
        CommonMethods.scrollElementIntoView(driver, link);
        CommonMethods.clickElement(driver, link);
    }
}