package com.mintel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.mintel.utilities.CommonMethods;
import com.mintel.utilities.ScenarioContext;

/**
 * This page contains methods and WebElements relating to the FAQ Page
 */
public class FaqPage extends BasePage
{
    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Use the constructor set within Base Page
     * <p>
     * @param driver The driver to set within the page
     */
    public FaqPage(WebDriver driver)
    {
        super(driver);
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Wait for the FAQ page to load
     */
    public void waitForFAQPage()
    {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".faq-main h1"))));
    }

    /**
     * Click on a section within the FAQ page
     * <p>
     * @param sectionName Section to click
     * @param scenarioContext Share test data
     */
    public void clickSection(String sectionName, ScenarioContext scenarioContext)
    {
        for (WebElement section : driver.findElements(By.cssSelector("h4")))
        {
            if (section.getText().contains(sectionName))
            {
                WebElement row = section.findElement(By.tagName("a"));
                CommonMethods.clickElement(driver, row);
                scenarioContext.setContext(ScenarioContext.Context.GENERAL.ATTRIBUTE, row.getAttribute("aria-expanded"));
            }
        }
    }
}