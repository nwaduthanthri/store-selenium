package com.mintel.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.mintel.utilities.CommonMethods;

/**
 * This page contains methods and WebElements relating to the checkout Page
 */
public class CheckoutPage extends BasePage
{
    /**
     * Checkout button
     */
    @FindBy(css = ".checkout-button")
    private WebElement checkoutButton;

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Use the constructor set within Base Page
     * <p>
     * @param driver The driver to set within the page
     */
    public CheckoutPage(WebDriver driver)
    {
        super(driver);
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Shop table WebElement
     * <p>
     * @return shopTable Return the shop table WebElement
     */
    public WebElement shopTable()
    {
        return driver.findElement(By.cssSelector(".shop_table"));
    }

    /**
     * Thank you order WebElement
     * <p>
     * @return thankYouOrder Return the thank you order WebElement
     */
    public WebElement thankYouOrder()
    {
        return driver.findElement(By.cssSelector("ul[class*='woocommerce-thankyou-order-details']"));
    }

    /**
     * Customer details WebElement
     * <p>
     * @return customerDetails Return the customer details WebElement
     */
    public WebElement customerDetails()
    {
        return driver.findElement(By.cssSelector(".woocommerce-customer-details"));
    }

    /**
     * Error message container WebElement
     * <p>
     * @return errorMessageContainer Return the error message container WebElement
     */
    public WebElement errorMessageContainer()
    {
        return driver.findElement(By.cssSelector(".woocommerce-error"));
    }

    /**
     * Download option WebElement
     * <p>
     * @return downloadOption Return the download option WebElement
     */
    public List<WebElement> downloadOption()
    {
        return driver.findElements(By.cssSelector(".download-file"));
    }

    /**
     * Get the price of a piece
     * <p>
     * @param pieceName Piece to get the price for
     * @return price Returns the price of the piece
     */
    public String getPrice(String pieceName)
    {
        return getPieceParent(pieceName).findElement(By.cssSelector(".product-subtotal bdi")).getText();
    }

    /**
     * Get the grand total
     * <p>
     * @return price Returns the grand total
     */
    public int getGrandTotal()
    {
        return (int) Double.parseDouble(driver.findElement(By.cssSelector("td[data-title='Grand Total'] bdi")).getText().replaceAll("[£,$€ ]", ""));
    }

    /**
     * Wait for the checkout button to load
     */
    public void waitForCheckoutButton()
    {
        wait.until(ExpectedConditions.visibilityOf(checkoutButton));
    }

    /**
     * Wait for the shop table to load
     */
    public void waitForShopTable()
    {
        wait.until(ExpectedConditions.visibilityOf(shopTable()));
    }

    /**
     * Click the checkout button
     */
    public void clickCheckoutButton()
    {
        CommonMethods.clickElement(driver, checkoutButton);
    }

    /**
     * Enter billing data into a field
     * <p>
     * @param fieldname The field to enter data into
     * @param data The data to enter into the field
     */
    public void enterBillingData(String fieldname, String data)
    {
        CommonMethods.enterTextIntoField(driver, data, driver.findElement(By.cssSelector("input[placeholder='" + fieldname + "']")));
    }

    /**
     * Enter text into search
     * <p>
     * @param textToEnter Text to enter
     */
    public void enterPaymentData(String fieldname, String textToEnter)
    {
        WebElement field = driver.findElement(By.cssSelector("input[placeholder='" + fieldname + "']"));
        CommonMethods.scrollElementIntoView(driver, field);
        for (int i = 0; i < textToEnter.length(); i++)
        {
            field.sendKeys(String.valueOf(textToEnter.charAt(i)));
        }
    }

    /**
     * Click a radio button
     * <p>
     * @param radioButton Radio button to click 
     */
    public void clickRadioButton(String radioButton)
    {
        CommonMethods.clickElement(driver, driver.findElement(By.xpath("//label[contains(text(),'" + radioButton + "')]")));
    }

    /**
     * Check conditions box
     */
    public void checkConditions()
    {
        CommonMethods.clickElement(driver, driver.findElement(By.cssSelector("input[name='terms']")));
    }

    /**
     * Wait for the thank you order element
     */
    public void waitForThankYouOrder()
    {
        wait.until(ExpectedConditions.visibilityOf(thankYouOrder()));
    }

    /**
     * Remove a piece from the cart
     * <p>
     * @param pieceName Piece to remove
     */
    public void removeItem(String pieceName)
    {
        CommonMethods.clickElement(driver, getPieceParent(pieceName).findElement(By.cssSelector(".product-remove a")));
    }

    /* **************** 
     *  PRIVATE METHODS 
     ****************** */

    /**
     * Get the parent of the piece
     * <p>
     * @param pieceName The piece's parent to get
     * @return pieceParent Return the parent of a piece
     */
    private WebElement getPieceParent(String pieceName)
    {
        WebElement parent = driver.findElement(By.tagName("div"));
        List<WebElement> products = driver.findElements(By.cssSelector(".cart_item .product-name"));
        for (WebElement productName : products)
        {
            if (productName.getText().contains(pieceName))
            {
                parent = productName.findElement(By.xpath(".."));
                break;
            }
            else
            {
                parent = products.get(products.size() - 1).findElement(By.xpath(".."));
                break;
            }
        }
        return parent;
    }
}