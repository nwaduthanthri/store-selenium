package com.mintel.pages;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;

import com.mintel.data.Constants.DateFormats;
import com.mintel.data.TestData.Email;
import com.mintel.utilities.CommonMethods;

/**
 * This page contains methods and WebElements relating to the store Login Page
 */
public class LoginPage extends BasePage
{
    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Use the constructor set within Base Page
     * <p>
     * @param driver The driver to set within the page
     */
    public LoginPage(WebDriver driver)
    {
        super(driver);
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Column element found on the login screen
     * <p>
     * @return columne2 Return the column WebElement
     */
    public WebElement column2()
    {
        return driver.findElement(By.cssSelector("#customer_login .col-2"));
    }

    /**
     * Generate a random email
     * <p>
     * @return email Return an email
     */
    public String generateEmail()
    {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateFormats.SSMMHHDDMMYYYY)).concat("@mintel.com");
    }

    /**
     * Generate a random password
     * <p>
     * @return password Return a password
     */
    public String generatePassword()
    {
        CharacterRule lowerCaseRule = new CharacterRule(EnglishCharacterData.LowerCase);
        lowerCaseRule.setNumberOfCharacters(5);
        CharacterRule upperCaseRule = new CharacterRule(EnglishCharacterData.UpperCase);
        upperCaseRule.setNumberOfCharacters(1);
        CharacterRule splCharRule = new CharacterRule(EnglishCharacterData.Special);
        splCharRule.setNumberOfCharacters(1);
        CharacterRule digitRule = new CharacterRule(EnglishCharacterData.Digit);
        digitRule.setNumberOfCharacters(1);
        return new PasswordGenerator().generatePassword(8, splCharRule, lowerCaseRule, upperCaseRule, digitRule);
    }

    /**
     * Click Login/Sign-up in the header
     */
    public void waitForLoginPage()
    {
        wait.until(ExpectedConditions.visibilityOf(column2()));
    }

    /**
     * Enter an email into the registration field
     * <p>
     * @param fieldData The data to enter into the field
     */
    public void enterRegistrationEmail(String fieldData)
    {
        CommonMethods.enterTextIntoField(driver, fieldData, driver.findElement(By.cssSelector("input[id='reg_email'], input[id='billing_email']")));
    }

    /**
     * Enter a password into the password field
     * <p>
     * @param fieldData The data to enter into the field
     */
    public void enterPassword(String fieldData)
    {
        if (fieldData.contentEquals(Email.TEST_CREDENTIALS))
        {
            CommonMethods.enterTextIntoField(driver, fieldData, driver.findElement(By.cssSelector("input[id='password']")));
        }
        else
        {
            CommonMethods.enterTextIntoField(driver, fieldData, driver.findElement(By.cssSelector("input[id='reg_password'], input[id='account_password']")));
        }
    }

    /**
     * Enter a username/email into the username/email field
     * <p>
     * @param fieldData The data to enter into the field
     */
    public void enterUsernameEmail(String fieldData)
    {
        action.moveToElement(driver.findElement(By.cssSelector("input[id='username'], input[id='user_login']"))).click().sendKeys(fieldData).perform();
    }

    /**
     * Click the forgotten password link
     */
    public void clickForgottenPasswordLink()
    {
        CommonMethods.clickElement(driver, driver.findElement(By.cssSelector(".lost_password a")));
    }

    /**
     * Wait for button name to appear
     * <p>
     * @param buttonName Button to wait for
     */
    public void waitForButton(String buttonName)
    {
        wait.until(ExpectedConditions.visibilityOf(buttonName(buttonName)));
    }
}