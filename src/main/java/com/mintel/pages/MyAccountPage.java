package com.mintel.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.mintel.utilities.CommonMethods;

/**
 * This page contains methods and WebElements relating to the store Account Page
 */
public class MyAccountPage extends BasePage
{
    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Use the constructor set within Base Page
     * <p>
     * @param driver The driver to set within the page
     */
    public MyAccountPage(WebDriver driver)
    {
        super(driver);
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Dashboard element found on the my account page
     * <p>
     * @return dashboard Return the dashboard WebElement
     */
    public WebElement dashboard()
    {
        return driver.findElement(By.cssSelector(".woocommerce h2"));
    }

    /**
     * Email field element found on the my account page
     * <p>
     * @return emailField Return the email field WebElement
     */
    public WebElement emailField()
    {
        return driver.findElement(By.cssSelector("input[name='account_email']"));
    }

    /**
     * Billing Address WebElement
     * <p>
     * @return billingAddress Return the billing address WebElement
     */
    public WebElement billingAddress()
    {
        return driver.findElement(By.cssSelector(".woocommerce-Address address"));
    }

    /**
     * Payment details WebElement
     * <p>
     * @return paymentDetails Return the payment details WebElement
     */
    public WebElement paymentDetails()
    {
        return driver.findElement(By.cssSelector(".woocommerce-MyAccount-paymentMethods"));
    }

    /**
     * Wait for the My Account Page to load
     */
    public void waitForMyAccountPage()
    {
        wait.until(ExpectedConditions.visibilityOf(dashboard()));
    }

    /**
     * Click on a row within the navigator
     * <p>
     * @param rowName Row to click
     */
    public void clickNavigatorRow(String rowName)
    {
        driver.findElement(By.cssSelector("nav[class='woocommerce-MyAccount-navigation']")).findElement(By.xpath("//a[text()='" + rowName + "']")).click();
    }

    /**
     * Wait for account email field
     */
    public void waitForAccountEmailField()
    {
        wait.until(ExpectedConditions.visibilityOf(emailField()));
    }

    /**
     * Hover over My Account
     */
    public void hoverOverMyAccount()
    {
        CommonMethods.hoverOverElement(driver.findElement(By.cssSelector(".myaccount")), driver);
    }

    /**
     * Wait for a row to appear within a drop-down and then click it
     * <p>
     * @param rowToClick Row to click within the drop-down
     */
    public void waitAndClickRow(String rowName)
    {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[text()='" + rowName + "']"))));
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[text()='" + rowName + "']")))).click();
    }

    /**
     * Click text
     * <p>
     * @param textToClick Text to click
     */
    public void clickText(String textToClick)
    {
        driver.findElement(By.xpath("//a[text()='" + textToClick + "']")).click();
    }

    /**
     * Edit address
     */
    public void addBillingAddress()
    {
        driver.findElement(By.cssSelector(".woocommerce-Address-title a")).click();
    }

    /**
     * Enter billing address data into a field
     * <p>
     * @param fieldname The field to enter data into
     * @param data The data to enter into the field
     */
    public void enterBillingAddressData(String fieldname, String data)
    {
        WebElement field = driver.findElement(By.xpath("//label[contains(text(),'" + fieldname + "')]/..")).findElement(By.tagName("input"));
        action.moveToElement(field);
        wait.until(ExpectedConditions.elementToBeClickable(field));
        field.clear();
        field.sendKeys(data);
    }
    
    /**
     * Select a country within a drop-down
     * <p>
     * @param country The country to select
     */
    public void selectCountry(String country)
    {
        driver.findElement(By.id("select2-billing_country-container")).click();
        action.moveToElement(driver.findElement(By.cssSelector("input[aria-owns='select2-billing_country-results']"))).sendKeys(country).sendKeys(Keys.ENTER).perform();
    }
}