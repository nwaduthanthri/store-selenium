package com.mintel.data;

/**
 * Store test data used across the project
 */
public class TestData
{
    /**
     * Email
     */
    public class Email
    {
        public static final String MINTEL_USER = "ndabasia@mintel.com";
        public static final String TEST_CREDENTIALS = "test@gmail.com";
    }
}