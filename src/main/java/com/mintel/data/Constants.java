package com.mintel.data;

/**
 * Store constant data used across the project
 */
public class Constants
{
    /**
     * Stores labels used across the store
     */
    public class StoreLabel
    {
        public static final String MINTEL_STORE = "Mintel Store";
        public static final String REGISTER = "Register";
        public static final String EMAIL_ADDRESS = "Email address";
        public static final String PASSWORD = "Password";
        public static final String MY_DASHBOARD = "My Dashboard";
        public static final String LOGIN_SIGNUP = "Login / Signup";
        public static final String USERNAME_EMAIL_ADDRESS = "Username or email address";
        public static final String SELECT_INDUSTRY = "Select Industry";
        public static final String ADD_TO_CART = "Add To Cart";
        public static final String FIRST_NAME = "First name";
        public static final String LAST_NAME = "Last name";
        public static final String TELEPHONE = "Telephone";
        public static final String PHONE = "Phone";
        public static final String HOUSE_NUMBER = "House number and street name";
        public static final String STREET_ADDRESS = "Street address";
        public static final String TOWN_CITY = "Town / City";
        public static final String STATE_COUNTRY = "State / County";
        public static final String POSTCODE = "Postcode / ZIP";
        public static final String COMPANY_NAME = "Company name";
        public static final String TEST_FIELD_CARD_DATA = "1234 1234 1234 1234";
        public static final String CARD_NUMBER = "Card Number";
        public static final String CVC = "CVC";
        public static final String MMYY = "MM / YY";
        public static final String CREDIT_CARD = "Pay Securely with Credit Card";
        public static final String DELETE_BUTTON = "Delete";
    }

    /**
     * Stores labels used across this project
     */
    public class ProjectLabel
    {
        public static final String BETA = "Beta";
        public static final String LIVE = "Live";
        public static final String REPORTS = "Reports";
        public static final String RESEARCH = "Research";
        public static final String CLASS = "class";
        public static final String LOADING = "loading";
        public static final String ACTIVE = "active";

    }

    /**
     * Stores URL strings
     */
    public class Url
    {
        public static final String BETA = "https://gmiller@mintel.com:ParKnotSharkAssist@storebeta.mintel.com/?utm_source=auto-test&utm_medium=auto-test/basic_auth";
        public static final String LIVE = "https://store.mintel.com/?utm_source=auto-test&utm_medium=auto-test";
    }

    /**
     * Stores messages
     */
    public class Messages
    {
        public static final String NO_ENVIRONMENT = "No environment specified";
    }

    /**
     * Date formats
     */
    public class DateFormats
    {
        public static final String SSMMHHDDMMYYYY = "ssmmHHddMMyyyy";
        public static final String MMYY = "MM/yy";
    }

    /**
     * Currencies
     */
    public class CURRENCY
    {
        public static final String EUR = "EUR";
        public static final String EURO = "€";
        public static final String DOLLAR = "$";
    }
}