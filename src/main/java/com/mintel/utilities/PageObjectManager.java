package com.mintel.utilities;

import org.openqa.selenium.WebDriver;

import com.mintel.pages.CheckoutPage;
import com.mintel.pages.FaqPage;
import com.mintel.pages.HomePage;
import com.mintel.pages.LoginPage;
import com.mintel.pages.MyAccountPage;
import com.mintel.pages.ProductLandingPage;
import com.mintel.pages.SearchResultsPage;

/**
 * Allows all the page objects to be declared here
 */
public class PageObjectManager
{
    private WebDriver driver;
    private HomePage homePage;
    private LoginPage loginPage;
    private MyAccountPage myAccountPage;
    private SearchResultsPage searchResultsPage;
    private ProductLandingPage productLandingPage;
    private CheckoutPage checkoutPage;
    private FaqPage faqPage;

    /**
     * Allows the driver to be set for each page
     * <p>
     * @param driver The driver to set within a page
     */
    public PageObjectManager(WebDriver driver)
    {
        this.driver = driver;
    }

    /**
     * Allow usage of Home Page's methods
     */
    public HomePage getHomePage()
    {
        return (homePage == null) ? homePage = new HomePage(driver) : homePage;
    }

    /**
     * Allow usage of Login Page's methods
     */
    public LoginPage getLoginPage()
    {
        return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
    }

    /**
     * Allow usage of My Account Page's methods
     */
    public MyAccountPage getMyAccountPage()
    {
        return (myAccountPage == null) ? myAccountPage = new MyAccountPage(driver) : myAccountPage;
    }

    /**
     * Allow usage of Search Results Page's methods
     */
    public SearchResultsPage getSearchResultsPage()
    {
        return (searchResultsPage == null) ? searchResultsPage = new SearchResultsPage(driver) : searchResultsPage;
    }

    /**
     * Allow usage of Product Landing Page's methods
     */
    public ProductLandingPage getProductLandingPage()
    {
        return (productLandingPage == null) ? productLandingPage = new ProductLandingPage(driver) : productLandingPage;
    }

    /**
     * Allow usage of Checkout Page's methods
     */
    public CheckoutPage getCheckoutPage()
    {
        return (checkoutPage == null) ? checkoutPage = new CheckoutPage(driver) : checkoutPage;
    }

    /**
     * Allow usage of FAQ Page's methods
     */
    public FaqPage getFaqPage()
    {
        return (faqPage == null) ? faqPage = new FaqPage(driver) : faqPage;
    }
}