package com.mintel.utilities;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;

/**
 * Used to assist in sharing states between step definitions
 */
public class ScenarioContext
{
    /**
     * Map between the Context variables and Object values set during the execution of a Scenario
     */
    private Map<Context, Object> scenarioContext;

    /**
     * Container to which scenario context data is stored
     */
    public interface Context
    {
        /**
         * Context relating to General words
         */
        enum GENERAL implements Context
        {
            TABLE_OF_CONTENT, PIECE_NAME, SECTION_NAME, ATTRIBUTE, PIECE_PRICE, SEARCH_RESULTS
        }

        /**
         * Context relating to Metadata
         */
        enum METADATA implements Context
        {
            INDUSTRY, KEYWORD, BILLING_DATA, PAYMENT_METHOD, CURRENCY, PAYMENT_DATA, EXPIRY_DATE
        }

        /**
         * Context relating to the types of Users
         */
        enum USERS implements Context
        {
            EMAIL
        }
    }

    /* **************** 
     *  CONSTRUCTOR 
     ****************** */

    /**
     * Hash map the scenario context
     */
    public ScenarioContext()
    {
        scenarioContext = new HashMap<>();
    }

    /* **************** 
     *  PUBLIC METHODS 
     ****************** */

    /**
     * Maps a value of type Object to a context key of type String
     * <p>
     * @param key Context enum of type String
     * @param value Object type
     */
    public void setContext(Context key, Object value)
    {
        scenarioContext.put(key, value);
    }

    /**
     * If the key has already been used, a value is added to that same key as a list otherwise it will be added to a new list
     * <p>
     * @param key Context enum of type String
     * @param value Object type
     */
    @SuppressWarnings("unchecked")
    public void setOrUpdateContextList(Context key, List<String> value)
    {
        ArrayList<String> list;
        if (scenarioContext.containsKey(key))
        {
            list = (ArrayList<String>) scenarioContext.get(key);
            list.addAll(value);
        }
        else
        {
            list = new ArrayList<String>();
            list.addAll(value);
            scenarioContext.put(key, list);
        }
    }

    /**
     * If the key has already been used, a value is added to that same key as a map otherwise it will be added to a new map
     * <p>
     * @param key Context enum of type String
     * @param value Object type
     */
    @SuppressWarnings("unchecked")
    public void setOrUpdateContextMap(Context key, Map<String, String> value)
    {
        HashMap<String, String> map;
        if (scenarioContext.containsKey(key))
        {
            map = (HashMap<String, String>) scenarioContext.get(key);
            map.putAll(value);
        }
        else
        {
            map = new HashMap<String, String>();
            map.putAll(value);
            scenarioContext.put(key, map);
        }
    }

    /**
     * Used to get the context of the enum context
     * <p>
     * @param key Takes key context enum as a parameter
     * @return context Returns the object which matches the key
     */
    public Object getContext(Context key)
    {
        return scenarioContext.get(key);
    }

    /**
     * Gets the Scenario context as a String list
     * <p>
     * @param key The scenario context to get
     * @return StringList Returns the Scenario context as a String list
     */
    public List<String> getScenarioContextAsStringList(Context key)
    {
        return getScenarioContextAsList(key, String.class);
    }

    /**
     * Gets the Scenario context as a String map
     * <p>
     * @param key The scenario context to get
     * @return listMapString Returns the Scenario context as a String map
     */
    public List<Map<String, String>> getScenarioContextAsStringListMap(Context key)
    {
        return getScenarioContextAsListMap(key, String.class);
    }

    /**
     * Gets the scenario context as a String
     * <p>
     * @param key Takes key context enum as a parameter
     * @return string Returns a String
     */
    public String getScenarioContextAsString(Context key)
    {
        Object value = this.getContext(key);
        if (!(value instanceof String))
        {
            fail();
        }
        return (String) value;
    }

    /**
     * Gets the Scenario context as a String map
     * <p>
     * @param key The scenario context to get
     * @return stringMap Returns the Scenario context as a String map
     */
    public Map<String, String> getScenarioContextAsStringMap(Context key)
    {
        return getScenarioContextAsMap(key, String.class);
    }

    /* **************** 
     *  PRIVATE METHODS 
     ****************** */

    /**
     * First check is to ensure the Object value is a list and the second check is to ensure that the list is not empty or that it contains a class type
     * <p>
     * @param key Takes key context enum as a parameter
     * @param type Class type
     * @return list Returns a List of generic type
     */
    @SuppressWarnings("unchecked")
    private <T> List<T> getScenarioContextAsList(Context key, Class<T> type)
    {
        Object value = this.getContext(key);
        if (!(value instanceof List<?>))
        {
            fail();
        }
        List<?> listMap = (List<?>) value;
        if (CollectionUtils.isEmpty(listMap) || !(listMap.get(0).getClass().isAssignableFrom(type)))
        {
            fail();
        }
        return (List<T>) listMap;
    }

    /**
     * First check is to ensure the Object value is a list, the second check is to ensure that the list Map is not empty or that it contains a map, the third check is to ensure the key contains a key by getting the first element in the set or the key is of class type and the final check is to ensure the values of the map contain a value by getting the first element in the set or the value is of class type.
     * <p>
     * @param key Takes key context enum as a parameter
     * @param type Class type
     * @return listMap Returns a List Map of generic type
     */
    @SuppressWarnings("unchecked")
    private <T> List<Map<T, T>> getScenarioContextAsListMap(Context key, Class<T> type)
    {
        Object value = this.getContext(key);
        if (!(value instanceof List<?>))
        {
            fail();
        }
        List<Map<?, ?>> listMap = (List<Map<?, ?>>) value;
        if (CollectionUtils.isEmpty(listMap) || !(listMap.get(0) instanceof Map<?, ?>))
        {
            fail();
        }
        Map<?, ?> map = (Map<?, ?>) listMap.get(0);

        if (!map.keySet().iterator().hasNext() || !map.keySet().iterator().next().getClass().isAssignableFrom(type))
        {
            fail();
        }
        if (!map.values().iterator().hasNext() || !map.values().iterator().next().getClass().isAssignableFrom(type))
        {
            fail();
        }
        return (List<Map<T, T>>) value;
    }

    /**
     * First check is to ensure the Object value is a map, the second check is to ensure that the Map is not empty.
     * <p>
     * @param key Takes key context enum as a parameter
     * @param type Class type
     * @return map Returns a Map of generic type
     */
    @SuppressWarnings("unchecked")
    private <T> Map<T, T> getScenarioContextAsMap(Context key, Class<T> type)
    {
        Object value = this.getContext(key);
        if (!(value instanceof Map<?, ?>))
        {
            fail();
        }
        if (CollectionUtils.sizeIsEmpty((Map<?, ?>) value))
        {
            fail();
        }
        return (Map<T, T>) value;
    }
}