# Store-Selenium

To run a scenario through eclipse, do the following:
1. Clone the repository into your local and open it through eclipe
2. Need to have the cucumber plugin installed in eclipse to be able to get the gherkin/step definitions working.
2. Choose a scenario thats within the feature file and add a @[tagname] at the top of the scenario
3. Click run, then run configurations from the header in eclipse
4. Find the feature file your scenario is in and click into it from the panel in the right
5. Click Arguements in the header once clicked into a feature file
6. Inside VM Arguements add the following -Dcucumber.options="--tags @[tagname]"
7. Click run

To run a test through docker instead of through eclipse
1. Add the following lines to the bottom of the docker file and then replace tagname with the tag of the scenario you would like to run.
2. Ensure when you are done, you comment it out again.

RUN mkdir /docker
WORKDIR /docker
CMD ["mvn", "test", "-Dcucumber.options=--tags @tagname"]


Overview of the code:

Data package:
* The Constants class is used to store variables that are used within the store or just used within the project
* The Test Data class is used to store test data that can be used within the store

Pages package:
* The BasePage class contains elements or methods that are common across all the pages of the store e.g. the login element in the header
* All other elements that are not common should be stored in its own page class. The page class should have page elements, interactions with the page (clicks, or getting text) and waits.

Utilities package:
* The CommonMethods class is used to store common methods used across the project and isn't tired down to a single page e.g. javascript interactions
* The DriverUtilities class is used to create and close the driver and currently determines whether the tests should be run in headless mode or not.
* Whenever a new page is created within the Pages package, a method within PageObjectManager class needs to be created that links the page to the driver, so that the methods within the newly created page can be utilised within the StepDefs package.
* The ScenarioContext class is used to share test data between step definitions. Use the setters to store the data and the getters to then retrieve it.
* The TestContext class is used to manage the DriverUtilities and PageObjectManager class

resources/features:
* The strucute of a test is written using Gherkin Syntax. At the top of each feature, you declare tags that identify this feature file. You then give a brief explanation of what the tests on that feature file are about using the "Feature" command. 
* The keywords to use when creating a test are either "Given, When or Then". "And" is used to extend the previous keyword command
* Given is used to set the test pre-condititions, When is used to indicate an action as part of the test and Then is used as a check to ensure the When action is done correctly.
* There are two types of scenarios that can be found in the FAQ feature file and the tableOfContents feature file. The tableOfContents feature file contains variables that are inputted on the gherkin line. The variable is stored below a key called "Row Name" and the test will run twice, one for each variable declared below "Row Name".
* Another example of a type of scenario can be found in the invoice feature file. Line 15 has data declared below the gherkin line, multiple rows at this level means it will carry out that line multiple times.

Hooks:
* The hooks class is used to determine what a test should do before and after it is run

StepDefs package:
* In the constructor, you should declare any page objects to be used within the class via the PageObjectManager class
* Classes in here are used to link the Gherkin syntax within the feature file with the page objects found within the page class
* Above each method within the class is the gherkin syntax with a Given/When/Then command to start it off.
* (.*) is used to indicate a variable that is declared in the gkerin syntax and is to be used within the code.
* DataTable is used to map tabular data as seen in line 15 of the invoice feature file to the code.
* Asserts should be done on this page to ensure a test passes or not

TestRunner:
* Here is where you set the file paths for the feature files that contains the gherkin syntax and the step definitions
